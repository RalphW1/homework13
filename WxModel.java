/**
 * Created by Tim McGowen on 3/22/2017
 *
 * Model to get weather information based on zipcode.
 */

/*
 * Name:       modified by Ralph Williams
 * Course:     CSCI0013
 * Date:       4/9/2018
 * Filename:   WxModel.java
 * Purpose:    Controller for the MVC version of extracting weather data  
 */

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import javafx.scene.image.Image;

public class WxModel {
  private JsonElement jse;
//  private final String apiKey = "340d3d982d67bef5";
   private final String apiKey = "ea202df8d65c8755";

  public boolean getWx(String zip)
  {
  
   System.out.println("The zip code entered is " + zip);
  
    try
    {
      URL wuURL = new URL("http://api.wunderground.com/api/" + apiKey +
          "/conditions/q/" + zip + ".json");

      // Open connection
      InputStream is = wuURL.openStream();
      BufferedReader br = new BufferedReader(new InputStreamReader(is));

      // Read the results into a JSON Element
      jse = new JsonParser().parse(br);

      System.out.println(jse);

      // Close connection
      is.close();
      br.close();
    }
    catch (java.io.UnsupportedEncodingException uee)
    {
      uee.printStackTrace();
    }
    catch (java.net.MalformedURLException mue)
    {
      mue.printStackTrace();
    }
    catch (java.io.IOException ioe)
    {
      ioe.printStackTrace();
    }
    catch (java.lang.NullPointerException npe)
    {
      npe.printStackTrace();
    }

    // Check to see if the zip code was valid.
    return isValid();
  }

   public boolean isValid()
   {
    // If the zip is not valid we will get an error field in the JSON
      try {
         String error = jse.getAsJsonObject().get("response").getAsJsonObject().get("error").getAsJsonObject().get("description").getAsString();
         return false;
      }

      catch (java.lang.NullPointerException npe)
      {
         // We did not see error so this is a valid zip
         return true;
      }
   }

   // Method for getting the location
   public String getLocation()
   {
      return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("display_location").getAsJsonObject().get("full").getAsString();
   }

   // Method for getting the temperature in F
   public double getTemp()
   {
      return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("temp_f").getAsDouble();
   }

   // Method for getting the ICON image
   public Image getImage()
   {
      String iconURL = jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("icon_url").getAsString();
      return new Image(iconURL);
   }
  
   // Method for getting the observation time
   public String getObserveTime()
   {
      return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("observation_time").getAsString();
   }
  
   // Method for getting the weather as a string
   public String getWeatherString()
   { 
      return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("weather").getAsString();
   }
   
   // Method for getting the wind as a string
   public String getWindString()
   { 
      return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("wind_string").getAsString();
   }

   // Method for getting the pressure as a string
   public String getPressure()
   {
      return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("pressure_in").getAsString();
   }
   
   // Method for getting the visibility in miles as a string
   public String getVisibility()
   {
      return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("visibility_mi").getAsString();
   }  
  
}
