/**
 * Created by Tim McGowen on 3/22/2017
 *
 * This is the controller for the FXML document that contains the view. 
 */

/*
 * Name:       modified by Ralph Williams
 * Course:     CSCI0013
 * Date:       4/9/2018
 * Filename:   WxController.java
 * Purpose:    Controller for the MVC version of extracting weather data  
 */ 


import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


public class WxController implements Initializable {

  @FXML
  private Button btngo;

  @FXML
  private TextField txtzipcode;

  @FXML
  private Label lblcity;

  @FXML
  private Label lbltemp;
  
  @FXML
  private Label lblObserveTime;

  @FXML
  private Label lblWeatherString;

  @FXML
  private Label lblTempF;

 @FXML
  private Label lblWindString;

  @FXML
  private Label lblPressure;
  
  @FXML
  private Label lblVisibility;
  
   @FXML
 private Image imgWeatherIcon;


  
 
  

  @FXML
  private ImageView iconwx;

  @FXML
  private void handleButtonAction(ActionEvent e) {
    // Create object to access the Model
    WxModel weather = new WxModel();

    // Has the go button been pressed?
    if (e.getSource() == btngo)
    {
      String zipcode = txtzipcode.getText();
      
      System.out.println("Zip code is " + zipcode);
      
      if (weather.getWx(zipcode))
      {
         lblcity.setText(weather.getLocation());
         lbltemp.setText(String.valueOf(weather.getTemp()));
         lblObserveTime.setText(weather.getObserveTime());
         lblWeatherString.setText(weather.getWeatherString());
         lblWindString.setText(weather.getWindString());
         lblPressure.setText(weather.getPressure());
         lblVisibility.setText(weather.getVisibility());
         iconwx.setImage(weather.getImage());
      }
      else
      {
        lblcity.setText("Invalid Zipcode");
        lbltemp.setText("");
        iconwx.setImage(new Image("badzipcode.png"));
      }
    }
  }

  @Override
  public void initialize(URL url, ResourceBundle rb) {
    // TODO
  }    

}


