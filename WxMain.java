/**
 * Created by Tim McGowen on 3/22/2017
 *
 * This code initiates the View and Controller.
 */

/*
 * Name:       modified by Ralph Williams
 * Course:     CSCI0013
 * Date:       4/9/2018
 * Filename:   WxModel.java
 * Purpose:    Controller for the MVC version of extracting weather data  
 */


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class WxMain extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("./WxView.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.setTitle("Wx - Ralph Williams");
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
